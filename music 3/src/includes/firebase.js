import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyBgcSjEpVikJbxO6b_EwjmKMzdOB5GMn2I",
  authDomain: "music-8256d.firebaseapp.com",
  projectId: "music-8256d",
  storageBucket: "music-8256d.appspot.com",
  appId: "1:1034261592874:web:2d0ad303288aa2fb09bc53",
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();
const db = firebase.firestore();
const storage = firebase.storage();

const usersCollection = db.collection("users");
const songsCollection = db.collection("songs");
const commentsCollection = db.collection("comments");

export {
  auth,
  db,
  usersCollection,
  storage,
  songsCollection,
  commentsCollection,
};
