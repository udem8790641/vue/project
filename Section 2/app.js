// "Vue.createApp()" Tells Vue to initialize application
// contains one arguement oject: the oject of configuration settings
// It is common practive to load a view application within an element rather than the whole page
// You can have numerous vue instances within a single page

const vm = Vue.createApp({
  data() {
    return {
      // JS expression - is a single line of code that evaluates to a VALUE. The value may be a number, string or a logical value
      firstName: "John",
      middleName: "",
      lastName: "Doe",
      url: "https://www.google.com/",
      raw_url: '<a href ="https://www.google.com/" target="_blank">Google</a>',
      age: 20,
    };
  },
  // The methods property - where we can define functions for our application
  // Methods are functions that you can use inside our vue instance or template functions
  methods: {
    // It is not recommended to use arrow functions because of the PROXY MECHANISM in Vue.
    // The 'this' keyword will not work in an arrow function. However, if you are not planning to use this keyword in your project, then you may use the arrow function
    increment() {
      this.age++;
    },
    // updateLastName(event) {
    //     this.lastName = event.target.value
    // }
    updateLastName(msg, event) {
      // event.preventDefault();
      // Vue allows EVENT MODIFIERS to modify the events in the project. By adding '.prevent' to @input in index.html, it also serves as 'event.preventDefault'

      console.log(msg);

      this.lastName = event.target.value;
    },
    updateMiddleName(event) {
      this.middleName = event.target.value;
    },
  },
  computed: {
    // Another thing you need to be aware of is that you cannot pass data onto the function. If you need to pass on data, then you may want to consider isong a method.
    fullName() {
      console.log("FullName method was called!");

      // Technically, this shouldn't work, but VUE will allow you to access the data like this thank to its PROXY MECHANISM
      return `${this.firstName} ${
        this.middleName
      } ${this.lastName.toUpperCase()}`;
    },
  },
  watch: {
    age(newVal, oldVal) {
      setTimeout(() => {
        this.age = 20;
      }, 3000);
    },
  },
}).mount("#app");

// setTimeout(() => {
//     // Technically, it is supposed to be 'vm.data().firstName'or 'vm.$data.firstName'. However, thanks to Vue's PROXY GAIN, Vue allows this 'shorcuts' and has taken this task (getters and setters) behind the scenes.
//     // Proxy - A figure that can eb sued to represent the vlue of something in a calculation.
//     vm.firstName = 'Bob';
// }, 2000)

// Mount - to insert a Vue application into the document
// Now we need to tell vue where to mount the application

// [Multiple Instances]
// Vue.createApp({
//     data() {
//         return {
//             firstName: 'Jane',
//             lastName: 'Doe'
//         }
//     }
// }).mount('#app2');
// End of multiple instances ====
